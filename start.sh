#!/bin/bash

# Exit on Error
set -e 
# macOS Only!
if [[ $(uname -s) != "Darwin" ]];then
	echo "FATAL ERROR"
	exit(1);
fi
# Install HomeBrew
echo "Installing Homebrew..."
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
if [[ -x $(which brew) ]];then
echo "Installing necessary USB-related libraries..."
fi
brew install libusbmuxd libimobiledevice https://raw.githubusercontent.com/kadwanev/bigboybrew/master/Library/Formula/sshpass.rb
read -p "Please jailbreak using checkra1n, and press enter "
echo "Starting the iproxy server..."
iproxy 2002 44
echo "Starting the ssh session..."
sshpass -p alpine ssh -o StrictHostKeyChecking=no root@localhost -p 2002 mount -o rw,union,update / && mv /Applications/Setup.app /Applications/Setup.app.bak && killall Setup && uicache --all && killall backboardd
exit 0
